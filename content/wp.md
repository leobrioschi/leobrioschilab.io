+++
title = "WORKING"
subtitle = "PAPERS"
+++

### **Political uncertainty on a daily basis: How the stock market reacts.**
- *I propose an index to measure political uncertainty on short time-frames and then study how the market reacts*  
- *Accepted on the World Finance Conference 2022 (Italy)*
- *Accepted on IFABS Conference 2022 (Italy)*


### **Don’t look around: Economic policy uncertainty spillover in the Latin America stock market.**
- *Looking at Economic Policy Uncertainty spillover in LatAm*


### **Can uncertainty signals from the policymaker infer stock market crises? A Brazilian anecdote.**
- *Economic Policy Uncertainty can help predict crises periods on the Brazilian stock market*
- [*Gitlab repo*](https://gitlab.com/leobrioschi/uncertainty-signals)
