+++
title = "Research"
subtitle = "Asset Pricing 'n Financial Statement Analysis"
+++

I am deeply interested in Asset Pricing and how the market reacts to different kinds of uncertainty. Shocks, quirks and irrational behavior enlightens who we are. Predictability and evaluating our assets tells us what efficient should look like.
  
## Working Papers  

### **Political uncertainty on a daily basis: How the stock market reacts.**
- *I propose an index to measure political uncertainty on short time-frames and then study how the market reacts*  
- *Accepted at the World Finance Conference 2022 (Italy)*
- *Accepted at IFABS Conference 2022 (Italy)*
- *Accepted at ISAFE 2022 (Vietnam)*
- *Presented at NTU's IFMB 2022 (Virtual, [Cert. Link](/pdf/brioschi-ifmb.pdf))*


### **Don’t look around: Economic policy uncertainty spillover in the Latin America stock market.**
- *Looking at Economic Policy Uncertainty spillover in LatAm*


### **Can uncertainty signals from the policymaker infer stock market crises? A Brazilian anecdote.**
- *Economic Policy Uncertainty can help predict crises periods on the Brazilian stock market*
- [*Gitlab repo*](https://gitlab.com/leobrioschi/uncertainty-signals)
